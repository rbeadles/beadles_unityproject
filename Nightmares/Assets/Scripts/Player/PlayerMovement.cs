﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
	public float speed = 6f;

	Vector3 movement;
	Animator anim;
	Rigidbody playerRigidbody;
	int floorMask;
	float camRayLength = 100f;

	public static int damagePerShot;
	public static float timeBetweenBullets;
	public static float range;
	public GameObject shotty;
	public GameObject mini;
	public GameObject snipper;
	public GameObject original;

	public AudioClip sniperShot;
	public AudioClip minigun;
	public AudioClip originalAudio;
	public AudioClip shotgun;


	public GameObject nextLevel;

	void Awake(){
		floorMask = LayerMask.GetMask ("Floor");
		anim = GetComponent<Animator> ();
		playerRigidbody = GetComponent<Rigidbody> ();
		damagePerShot = 20;
		timeBetweenBullets = .15f;
		range = 100f;
	}

	void FixedUpdate(){
		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");

		Move (h, v);
		Turning ();
		Animating (h, v);


	}

	void Move (float h, float v){
		movement.Set (h, 0f, v);

		movement = movement.normalized * speed * Time.deltaTime;

		playerRigidbody.MovePosition (transform.position + movement);
	}

	void Turning(){
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);

		RaycastHit floorHit;

		if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask)) {
			Vector3 playerToMouse = floorHit.point - transform.position;
			playerToMouse.y = 0f;

			Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
			playerRigidbody.MoveRotation (newRotation);
					}
	}

	void Animating (float h, float v){
		bool walking = h!=0f || v!=0f;
		anim.SetBool ("IsWalking", walking);

	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag("ShotGun")){
			damagePerShot = 34;
			timeBetweenBullets = .37f;
			range = 10f;

			AudioSource audio = transform.GetChild(1).GetComponent<AudioSource> ();
			audio.clip = shotgun;

			shotty = GameObject.FindGameObjectWithTag ("ShotGun");
			Vector3 placement = shotty.transform.position;
			shotty.transform.position = new Vector3 (1000f, 1000f, 1000f);
			StartCoroutine (timer (shotty, placement));




		}
		if (other.CompareTag("MiniGun")){
			damagePerShot = 7;
			timeBetweenBullets = .05f;
			range = 30f;

			AudioSource audio = transform.GetChild(1).GetComponent<AudioSource> ();
			audio.clip = minigun;

			mini = GameObject.FindGameObjectWithTag ("MiniGun");
			Vector3 placement = mini.transform.position;
			mini.transform.position = new Vector3 (1000f, 1000f, 1000f);
			StartCoroutine (timer (mini, placement));
		}

		if (other.CompareTag("Snipper")){
			damagePerShot = 100;
			timeBetweenBullets = .75f;
			range = 200f;

			AudioSource audio = transform.GetChild(1).GetComponent<AudioSource> ();
			audio.clip = sniperShot;

			snipper = GameObject.FindGameObjectWithTag ("Snipper");
			Vector3 placement = snipper.transform.position;
			snipper.transform.position = new Vector3 (1000f, 1000f, 1000f);
			StartCoroutine (timer (snipper, placement));
		}

		if (other.CompareTag("Default")){
			damagePerShot = 20;
			timeBetweenBullets = .15f;
			range = 100f;

			AudioSource audio = transform.GetChild(1).GetComponent<AudioSource> ();
			audio.clip = originalAudio;

			original = GameObject.FindGameObjectWithTag ("Default");
			Vector3 placement = original.transform.position;
			original.transform.position = new Vector3 (1000f, 1000f, 1000f);
			StartCoroutine (timer (original, placement));
		}

		if (other.CompareTag ("Level")) {
			sceneLoad ();

		}

	}

	IEnumerator timer(GameObject weapon, Vector3 placement){
		//gets weapon and location before it is moved
		yield return new WaitForSeconds(3);
		//waits three seconds and brings it back to original location
		weapon.transform.position = placement;
	}

	void sceneLoad(){
		SceneManager.LoadScene ("Level_02");
	}






}
