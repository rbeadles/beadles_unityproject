﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScoreManager : MonoBehaviour {

	int highScore = 0;	

	Text text;

	void Awake(){
		text = GetComponent <Text> ();
		highScore = PlayerPrefs.GetInt ("highScore", 0);
	}

	void Update(){
		if (highScore < ScoreManager.score) {
			highScore = ScoreManager.score;
			PlayerPrefs.SetInt ("highScore", highScore);
			text.text = "New High Score: " + highScore;
		} else {
			text.text = "High Score: " + highScore;

		}
	}
}
