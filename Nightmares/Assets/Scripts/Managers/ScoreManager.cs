﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class ScoreManager : MonoBehaviour
{
    public static int score;
    Text text;
	public GameObject nextLevel;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
		if (score < 1000) {
			text.text = "Score: " + score + "/1000";
		} else {
			if (GameObject.Find("nextLevel(Clone)") == null)
			{
				Instantiate (nextLevel);
			}						
			text.text = "Complete Your Nightmare";
		}

	}
}
