﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();

		if (ScoreManager.score >= 100 && ScoreManager.score < 200) {
			gameObject.transform.localScale += new Vector3 (.3f, .3f, .3f);
		} else if (ScoreManager.score >= 200 && ScoreManager.score < 300) {
			gameObject.transform.localScale += new Vector3 (.6f, .6f, .6f);
		} else if (ScoreManager.score >= 300 && ScoreManager.score < 400) {
			gameObject.transform.localScale += new Vector3 (.9f, .9f, .9f);
		} else if (ScoreManager.score >= 400) {
			gameObject.transform.localScale += new Vector3 (1.5f, 1.5f, 1.5f);
		}

    }


    void Update ()
    {
        if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            nav.SetDestination (player.position);
        }
        else
        {
            nav.enabled = false;
        }
    }
}
